# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from elasticsearch import Elasticsearch


class RentScraperPipeline(object):
    def __init__(self):
        self.es = Elasticsearch()

    def process_item(self, item, spider):
        """
        For each item yielded by the Imovirtual spider, insert it into
        ElasticSearch running on localhost:9200.
        """
        res = self.es.index(index='rent', doc_type='item', body=item)

        return item
