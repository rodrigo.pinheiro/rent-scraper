# -*- coding: utf-8 -*-
import scrapy
import os


class ImovirtualSpider(scrapy.Spider):
    name = 'imovirtual'
    allowed_domains = ['imovirtual.com']
    start_urls = ['https://www.imovirtual.com/arrendar/apartamento/lisboa/'
                  '?search%5Bsubregion_id%5D=153']
    page = 1
    max_pages = 50

    def parse(self, response):
        """
        Parses Imovirtual's search results page. For all pages this method gets
        a list of unique links to available houses for rent and for each one
        calls asynchronously the parse_item method.
        """

        # Get a list of house's urls
        urls = list(set(response.xpath(
            '//a[@data-featured-name="listing_no_promo"]/@href').extract()))

        # For each one url call the parse_item method
        for url in urls:
            yield response.follow(url, callback=self.parse_item)

        # Get next search results
        if urls:
            self.page += 1
            next_page_url = os.path.join(
                self.start_urls[0],
                '&page={}'.format(self.page)
            )

            if self.page < self.max_pages:
                yield response.follow(next_page_url, callback=self.parse)

    def parse_item(self, response):
        """
        Parses and sanitizes relvant information from the house for rent page.
        At the end, this method yields a dictionary with all the information
        gathered.
        """
        # Get the house title
        title = response.xpath('//h1/text()').extract_first()

        # Get the house location
        location = response.xpath(
            '//a[@class="css-1ovw93c-baseStyle-AdPage-contentStyle"]/text()'
        ).extract_first().split(',')[0]

        # Get the renting price
        price = response.xpath(
            '//div[@class="css-7ryazv-AdHeader-className"]/text()'
        ).extract_first()
        price = price.replace(' ', '').strip('€')

        # Get the price per area
        price_per_area = response.xpath(
            '//div[@class="css-g24dpp-AdHeader-className"]/text()'
        ).extract_first()
        price_per_area = price_per_area.replace(' ', '').strip('€/m²')

        # Parse more features that are displayed on a html list
        features = {}
        for feature in response.xpath(
                '//div[@class="css-838ksj-AdOverview-className"]/ul/li'):
            key = feature.xpath('./text()').extract_first().strip().strip(':')
            value = feature.xpath('./strong/text()').extract_first()

            if key.startswith('Área'):
                value = value.replace(' ', '').strip('m²')

            features[key] = value

        # Build the data and yield the final result
        yield {
            'title': title,
            'location': location,
            'price': float(price),
            'price_per_area': float(price_per_area),
            'features': features
        }
