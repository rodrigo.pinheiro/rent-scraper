A simple scraping tutorial using [Scrapy](https://scrapy.org/) alongside with [Elasticsearch](https://www.elastic.co/products/elasticsearch) and [Kibana](https://www.elastic.co/products/kibana).

Getting started
===============


Installation
------------
Create virtual environment using Python 3 (preferably version 3.6):
```
virtualenv -p python3 --no-site-packages ~/.virtualenvs/rent_scraper
```

Activate virtualenv:
```
source ~/.virtualenvs/rent_scraper/bin/activate
```

Clone the project:
````
git clone https://gitlab.com/rodrigo.pinheiro/rent-scraper.git 
````

cd into the cloned directory and install all the python libs requirements:
```
pip install -r requirements.txt
```

Run the dockers needed for the demo:
```
cd docker
mkdir data
docker-compose up
```

if you have the following error running the containers on linux:
```
rent-elasticsearch | [1]: max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]
```
run the following command:
```
sudo sysctl -w vm.max_map_count=262144
```

Run the scrapers:
```
cd ../rent_scraper
scrapy crawl imovirtual
```

Finally, open Kibana to explore the data on http://localhost:5601

To setup the new index do the followin:

- On Kibana go to Management -> Kibana -> Index Pattners
- search by 'rent' and click on "Next step"
- finally click on "Create Index pattern"


To import the Kibana dashboard, do the following:

- on Kibana go to Management -> Saved Objects -> Import
- Open the file docker/dashboard.json to import the new dashboard.